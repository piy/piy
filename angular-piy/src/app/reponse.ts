import { BigInteger } from 'jsbn';

export class Reponse{
	code: string;
	value: string;
}

export class ReponseClear{
	code: string;
	value: number;

	constructor(sk: any, code: string, value: string){
		this.code = code;
		const bn = new BigInteger(value);
		this.value = +sk.decrypt(bn).toString();
	}
}
