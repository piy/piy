import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  /**
   * Nivigue vers la vue de création d'un sondage
   */
  onCreateClick(){
    this.router.navigate(['create']);
  }

  /**
   * Navigue vers la vue listant les sondages
   */
  onListClick(){
    this.router.navigate(['list']);
  }
}
