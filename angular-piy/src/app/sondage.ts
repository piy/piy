import { Question } from './question';

export class Sondage {
  id: number;
  intitule: string;
  publie: boolean;
  termine: boolean;
  nawait: number;
  nagreg: number;
  questions: Question[] = [];
}
