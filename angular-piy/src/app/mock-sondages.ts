import { Sondage } from './sondage';
import { QUESTIONS } from './mock-questions';

export const SONDAGES: Sondage[] = [
  {id: 1, intitule: "Avez-vous eu des effets secondaires avec le chocolat ?", questions: QUESTIONS[0], publie: true, termine: false, nawait: 0, nagreg: 0},
  {id: 2, intitule: "Rhume et chocolat sont-ils liés ?", questions: QUESTIONS[1], publie: true, termine: false, nawait: 0, nagreg: 0},
  {id: 3, intitule: "Le chocolat rend-t'il bete ?", questions: QUESTIONS[2], publie: true, termine: false, nawait: 0, nagreg: 0},
  {id: 4, intitule: "Faut-il etre breton pour etre protégé du mal de tete ?", questions: QUESTIONS[3], publie: true, termine: false, nawait: 0, nagreg: 0},
  {id: 5, intitule: "Etre breton est-il un facteur de beauté ?", questions: QUESTIONS[4], publie: true, termine: false, nawait: 0, nagreg: 0},
  {id: 6, intitule: "42 ?", questions: QUESTIONS[5], publie: true, termine: false, nawait: 0, nagreg: 0},
  {id: 7, intitule: "Pince-mi et pince-moi sont sur un bateau. Pince-mi tombe à l'eau. Qui il reste ?", questions: QUESTIONS[6], publie: true, termine: false, nawait: 0, nagreg: 0},
  {id: 8, intitule: "L'hiver est-il dangereux pour la santé ?", questions: QUESTIONS[7], publie: true, termine: false, nawait: 0, nagreg: 0},
  {id: 9, intitule: "Chocolat ou cholatine ?", questions: QUESTIONS[8], publie: true, termine: false, nawait: 0, nagreg: 0},
  {id: 10, intitule: "Barcelone ou Real Madrid ?", questions: QUESTIONS[9], publie: true, termine: false, nawait: 0, nagreg: 0}
];
