import { Component, OnInit } from '@angular/core';
import { SondageService } from '../sondage.service';
import { Sondage } from '../sondage' ;

@Component({
  selector: 'app-liste-sondage',
  templateUrl: './liste-sondage.component.html',
  styleUrls: ['./liste-sondage.component.css']
})
export class ListeSondageComponent implements OnInit {
  sondages: Sondage[] = [];
  search: string = "";
  publie: boolean = true;
  termine: boolean = false;
  page: number = 1 ;
  ps: number = 5;
  nbSond: number ;
  nbPages: number ;

  constructor(private sondageService : SondageService) { }

  ngOnInit() {
    this.getSondages();
  }

  getSondages(): void {
    this.sondageService
      .list(this.search, this.page, this.ps, this.publie, this.termine)
      .subscribe(page => {
      	this.sondages = page.results;
      	this.nbSond = page.count;
      //  this.nbPages = Math.ceil(this.nbSond/this.ps);
      }
      	);
  }

  //fakeArray = new Array(this.nbPages);

  next(): void
  {
  	if(this.page < this.nbSond/this.ps)
  	{
  		this.page+=1;
  	}

  	this.getSondages();
  }

  previous(): void
  {
  	if(this.page>1)
  	{
  		this.page-=1;
  	}

  	this.getSondages();
  }

}
