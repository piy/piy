import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { apiurl } from './config/config';
import { PublicKey, SecretKey } from './keys';

const httpOptions = {
  headers: new HttpHeaders({
    'Cache-control': 'no-cache',
    'Pragma': 'no-cache',
    'Expires': 'Sat, 01 Jan 2000 00:00:00 GMT',
  })
};

@Injectable()
export class KeysService {

  constructor(private http: HttpClient) { }

  private url(sondage: number): string{
  	return apiurl + 'sondages/' + sondage + '/keys/';
  }

  private url_public(sondage: number): string{
  	return this.url(sondage) + 'public/';
  }

  private url_secret(sondage: number): string{
  	return this.url(sondage) + 'secret/';
  }

  create(sondage: number, sec: SecretKey): Observable<SecretKey>{
  	return this.http.post<SecretKey>(this.url(sondage), sec, httpOptions);
  }

  public(sondage: number): Observable<PublicKey>{
  	return this.http.get<PublicKey>(this.url_public(sondage), httpOptions);
  }

  secret(sondage: number): Observable<SecretKey>{
    console.log
  	return this.http.get<SecretKey>(this.url_secret(sondage), httpOptions);
  }
}
