import { Component, OnInit } from '@angular/core';
import { SondageService } from '../sondage.service';
import { Sondage } from '../sondage' ;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  sondages: Sondage[] = [];
  search: string = "";
  publie: boolean = true;
  termine: boolean = false;
  page: number = 1 ;
  ps: number = 5;
  nbSond: number ;
  nbPages: number ;

  constructor(private sondageService : SondageService) { }

  ngOnInit() {
  }

  getSondages(): void {
    this.sondageService
      .list(this.search, this.page, this.ps, this.publie, this.termine)
      .subscribe(page => {
          this.sondages = page.results;
          this.nbSond = page.count;
      //  this.nbPages = Math.ceil(this.nbSond/this.ps);
      });
  }

}
