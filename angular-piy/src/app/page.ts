/* Réponse paginée contentant des éléments de type T*/
export class Page<T>{
	// nombre total d'éléments (non paginé)
	count: number;
	// url page suivante (null si aucune page disponible)
	next: string;
	// page précédente (null si aucune page disponible)
	previous: string;
	// éléments dans la page
	results: T[]
}