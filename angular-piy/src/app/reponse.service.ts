import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError } from 'rxjs/operators';

import { apiurl } from './config/config';
import { Reponse } from './reponse';

const httpOptions = {
  headers: new HttpHeaders({
    'Cache-control': 'no-cache',
    'Pragma': 'no-cache',
    'Expires': 'Sat, 01 Jan 2000 00:00:00 GMT',
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class ReponseService {

  constructor(private http: HttpClient) { }


  private url(sondage: number): string{
  	return apiurl + "sondages/" + sondage + "/reponses/";
  }

  private url_detail(sondage: number, code: string): string{
  	return this.url(sondage) + code + "/";
  }

  /* Liste les réponses possible au sondage */
  list(sondage: number): Observable<Reponse[]>{
  	return this.http.get<Reponse[]>(this.url(sondage), httpOptions);
  }

  /* Ajoute une réponse au sondage */
  create(sondage: number, code: string, val: number): Observable<string>{
  	const data = {
  		code: code,
  		value: val
  	};

  	return this.http.post<string>(this.url(sondage), JSON.stringify(data), httpOptions)
    .pipe(
      catchError(this.handleError<string>('create response', ''))
      );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);

      return of(result as T);
    };
  }
}
