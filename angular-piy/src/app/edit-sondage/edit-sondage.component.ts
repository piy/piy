import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import * as paillier from 'jspaillier';
import { SecretKey, sk_from_jspaillier } from '../keys'
import { KeysService } from '../keys.service'
import { Sondage } from '../sondage';
import { SondageService } from '../sondage.service';
import { Question } from '../question';
import { QuestionService } from '../question.service';
import { url } from '../config/config'; 

@Component({
  selector: 'app-edit-sondage',
  templateUrl: './edit-sondage.component.html',
  styleUrls: ['./edit-sondage.component.css'],
})
export class EditSondageComponent implements OnInit {
  status: number = 0;
  sondage: Sondage;
  questions: Question[] = [];
  keys: SecretKey = new SecretKey(1024, null, null);
  url_sondage = url + '/reponse/';

  constructor(
    private sondageService : SondageService,
    private questionService: QuestionService,
    private keysService: KeysService,
    private route: ActivatedRoute,
    private router:Router
  ) { }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');

    this.sondageService
      .detail(id)
      .subscribe(
        sondage => {
          this.status = 1;
          this.sondage = sondage;
          this.loadPollElements(sondage);
        },
        err => {
          if (err.status == 404){
            this.status = 2;
          }
        }
      );
  }

  loadPollElements(sondage){
    this.questionService
      .list(sondage.id)
      .subscribe(questions => this.questions = questions);

    this.keysService
      .secret(sondage.id)
      .subscribe(
        keys => {
          this.keys = keys;
        },
        err => {
          this.keys = new SecretKey(1024, null, null)
        }
      );
  }

  /* Reponse URL */
  reponseURL(): string{
    return url + 'reponse/' + this.sondage.id;
  }

  /* Publie un sondage non publié */
  publish(){
    if (this.sondage.publie == false && this.questions.length == 2){
      this.sondageService.publish(this.sondage.id).subscribe(res => {
        this.sondage.publie = true;
      });
    }
  }

  /* Test if the publish button should be enables */
  enablePublish(){
    return this.questions.length == 2 && this.keys.n != null;
  }

  /* Termine un sondage non terminé*/
  terminate(){
    if (this.sondage.termine == false){
      this.sondageService.terminate(this.sondage.id).subscribe(res => {});
      this.sondage.termine = true;
    }
  }

  /* Supprime le sondage */
  supprime(){
    this.sondageService.delete(this.sondage.id).subscribe(res => {
      this.router.navigate(['list']);
    });
  }

  /* Ajoute une question au sondage */
  ajoutQuestion(intitule){
    if (!this.sondage.publie){
      this.questionService.create(this.sondage.id, intitule.value).subscribe(question => {
        this.questions.push(question);
        intitule.value = "";
      });
    }
  }

  /* Remove the question */
  remQuestion(id){
    if (!this.sondage.publie){
      this.questionService.delete(this.sondage.id, id).subscribe(res => {
        this.questions = this.questions.filter(question => question.id != id);
      });
    }
  }

  /* Generete a new pair of keys for the poll (if not published) */
  genKeys(){
    if (this.keys.size >= 512 && this.keys.size <= 8192){
      const jskeys = paillier.generateKeys(this.keys.size);
      const keys = sk_from_jspaillier(jskeys.sec);

      this.keysService.create(this.sondage.id, keys).subscribe(keys=>{
        this.keys = keys;
      });
    }
  }
}
