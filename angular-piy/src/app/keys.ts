import * as paillier from 'jspaillier';
import { BigInteger } from 'jsbn';

/* A paillier public key as handled by the server */
export class PublicKey {
	// key size (in bits)
	size: number;
	// n (public) key component
	n: string;

	constructor(size: number, n: string){
		this.size = size;
		this.n = n;
	}
}

/* A paillier secret key as handled by the server */
export class SecretKey extends PublicKey{
	// lambda (secret) key component
	lmbda: string;

	constructor(size: number, n: string, lmbda: string){
		super(size, n);
		this.lmbda = lmbda;
	}
}

/* Load this key as a jspaillier representation */
export function pk_to_jspaillier(key: PublicKey): paillier.publicKey {
	const size = key.size;
	const n = new BigInteger(key.n);
	return new paillier.publicKey(size, n);
}

/* Load this key as a jspaillier representation */
export function sk_to_jspaillier(key: SecretKey): paillier.secretKey{
	const pub = pk_to_jspaillier(key);
	const lmbda = new BigInteger(key.lmbda);
	return new paillier.privateKey(lmbda, pub);
}

/* Load a key from the jspailler representation */
export function pk_from_jspaillier(pub: paillier.publicKey): PublicKey{
	return new PublicKey(pub.bits, pub.n.toString());
}

/* Load a key from the jspailler representation */
export function sk_from_jspaillier(sec: paillier.secretKey): SecretKey{
	return new SecretKey(sec.pubkey.bits, sec.pubkey.n.toString(), sec.lambda.toString());
}
