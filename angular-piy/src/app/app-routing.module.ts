import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreerSondageComponent} from './creer-sondage/creer-sondage.component';
import { ListeSondageComponent} from './liste-sondage/liste-sondage.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AfficherSondageComponent } from './afficher-sondage/afficher-sondage.component';
import { EditSondageComponent } from './edit-sondage/edit-sondage.component';
import { RepondreSondageComponent } from './repondre-sondage/repondre-sondage.component';
import { ThankyouComponent } from './thankyou/thankyou.component';

/**
 * Vos routes doivent se retrouver ici. N'oubliez pas d'importer vos component !!!
 * format de base d'une route:
 * {path: 'mon/schema/d/url', component: MonComponent}
 */
const routes: Routes = [
  {path: 'create', component: CreerSondageComponent},
  {path: 'list', component: ListeSondageComponent},
  {path: 'home', component: HomeComponent},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'sondage/:id', component: AfficherSondageComponent},
  {path: 'edit/:id', component: EditSondageComponent},
  {path: 'reponse/:id', component: RepondreSondageComponent},
  {path: 'thankyou', component: ThankyouComponent},
  {path: '', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
