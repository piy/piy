import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { QRCodeModule } from 'angularx-qrcode';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { ListeSondageComponent } from './liste-sondage/liste-sondage.component';
import { CreerSondageComponent } from './creer-sondage/creer-sondage.component';
import { SondageService } from './sondage.service';
import { QuestionService } from './question.service';
import { ReponseService } from './reponse.service';
import { KeysService } from './keys.service'

import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AfficherSondageComponent } from './afficher-sondage/afficher-sondage.component';
import { EditSondageComponent } from './edit-sondage/edit-sondage.component';
import { RepondreSondageComponent } from './repondre-sondage/repondre-sondage.component';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { AppBoostrapModule } from './app-boostrap.module';
import { NavComponent } from './nav/nav.component';
import { CarouselModule, ButtonsModule } from 'ngx-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    ListeSondageComponent,
    CreerSondageComponent,
    HomeComponent,
    DashboardComponent,
    AfficherSondageComponent,
    EditSondageComponent,
    RepondreSondageComponent,
    ThankyouComponent,
    NavComponent
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    QRCodeModule,
    AppBoostrapModule,
    CarouselModule,
    ButtonsModule
  ],

  providers: [
    SondageService,
    QuestionService,
    ReponseService,
    KeysService
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
