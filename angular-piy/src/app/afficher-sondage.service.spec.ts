import { TestBed, inject } from '@angular/core/testing';

import { AfficherSondageService } from './afficher-sondage.service';

describe('AfficherSondageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AfficherSondageService]
    });
  });

  it('should be created', inject([AfficherSondageService], (service: AfficherSondageService) => {
    expect(service).toBeTruthy();
  }));
});
