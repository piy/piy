import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, tap } from 'rxjs/operators';

import { apiurl } from './config/config';
import { Question } from './question';
import { QUESTIONS } from './mock-questions';

const httpOptions = {
  headers: new HttpHeaders({
    'Cache-control': 'no-cache',
    'Pragma': 'no-cache',
    'Expires': 'Sat, 01 Jan 2000 00:00:00 GMT',
  })
};

@Injectable()
export class QuestionService {

  constructor(private http: HttpClient) { }

  private url(sondage_id: Number): string{
    return apiurl + 'sondages/' + sondage_id + "/questions/";
  }

  private url_detail(sondage_id, id: Number): string{
    return this.url(sondage_id) + id + '/'
  }

  /* Liste toutes les questions d'un sondage */
  list(sondage: Number):Observable<Question[]>{
    return this.http.get<Question[]>(this.url(sondage), httpOptions)
      .pipe(
        catchError(this.handleHerror("list sondage", []))
      );
  }

  /* Ajoute une question au sondage */
  create(sondage: Number, intitule: String): Observable<Question>{
    const data = {
      intitule: intitule
    };

    return this.http.post<Question>(this.url(sondage), data, httpOptions);
  }

  /* Obtient les détails d'une question appartenant à un sondage */
  detail(sondage: Number, question: Number): Observable<Question>{
    return this.http.get<Question>(this.url_detail(sondage, question), httpOptions);
  }

  /* Supprime la question du sondage */
  delete(sondage: Number, question: Number): Observable<any>{
    return this.http.delete(this.url_detail(sondage, question), httpOptions);
  }

  private handleHerror<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);

      return of(result as T);
    }
  }

}
