import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { Sondage } from '../sondage';
import { SondageService } from '../sondage.service'

@Component({
  selector: 'app-creer-sondage',
  templateUrl: './creer-sondage.component.html',
  styleUrls: ['./creer-sondage.component.css']
})

export class CreerSondageComponent implements OnInit {

  	constructor(
      private sondageService: SondageService,
      private router: Router
    )
    { }

  	ngOnInit() {
  	}

  	creerSondage(intitule: string): void {
      this.sondageService
  			.create(intitule)
  			.subscribe(sondage => {
          this.router.navigate(['edit', sondage.id]);
        });
  	}
}
