import { Question } from './question';

export const QUESTIONS: Question[][] = [
  [{id: 1, num: 1, intitule: 'A noel ?'}, {id: 2, num: 2, intitule: 'A paque ?'}],
  [{id: 3, num: 1, intitule: 'Rhume et chocolat sont-ils liés ?'}],
  [{id: 4, num: 1, intitule: 'Avez-vous déjà regardé les anges de la téléréalité après avoir mangé du chocolat ?'}, {id: 5, num: 2, intitule: 'Avez-vous regardé les transformés de Fourier après avoir mangé du chocolat ?'}],
  [{id: 7, num: 1, intitule: 'Buvez-vous de l\'alcool sans avoir mal à la tete ?'}, {id: 8, num: 2, intitule: 'Avez-vous déjà reçu un dolmen sur la tete sans avoir mal ?'}],
  [{id: 9, num: 1, intitule: 'Boire rend-t\'il plus beau ?'}, {id: 10, num: 2, intitule: 'Les marinières sont-elles un facteur de beauté ?'}],
  [{id:11, num:1, intitule: '42 ?'}, {id:12, num:2, intitule: 'Or not 42 ?'}],
  [{id:13, num:1, intitule: 'Pince-moi ?'}, {id:14, num:2, intitule: 'Pince-moi ?'}],
  [{id:15, num:1, intitule: 'Avez-vous déjà eu une grippe en hiver ?'}, {id:16, num:2, intitule: 'Avez-vous déjà eu une gastro en hiver ?'}],
  [{id:17, num:1, intitule: 'Pain au chocolat ?'}, {id:18, num:2, intitule: 'Chocolatine ?'}],
  [{id:19, num:1, intitule: 'Barcelone ?'}, {id:20, num:2, intitule: 'Real Madrid ?'}],
];