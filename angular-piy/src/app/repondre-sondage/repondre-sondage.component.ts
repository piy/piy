import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Reponse } from '../reponse';
import { Question } from '../question';
import { PublicKey, pk_to_jspaillier } from '../keys';
import { SondageService } from '../sondage.service';
import { ReponseService } from '../reponse.service';
import { QuestionService } from '../question.service';
import { KeysService } from '../keys.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BigInteger } from 'jsbn';

@Component({
  selector: 'app-repondre-sondage',
  templateUrl: './repondre-sondage.component.html',
  styleUrls: ['./repondre-sondage.component.css']
})
export class RepondreSondageComponent implements OnInit {

	public intitule: string = '';
	public questions: Question[] = [];
	private id: number;
  private keys: PublicKey;
  private inc: number = 0;

	/**
	 * @param {ActivatedRoute} route - A service to get some usefull information about the current URI
	 * @param {Router} router - A usefull service to do some things with router module, example: a redirection
	 * @param {QuestionService} reponseService - A service to recover questions
	 * @param {ReponseService} questionService - A service to send a response.
	 */
  constructor(
  	private route: ActivatedRoute,
  	private router: Router,
  	private questionsService: QuestionService,
  	private sondageService: SondageService,
  	private reponseService: ReponseService,
    private keysService: KeysService
  ) { }

  ngOnInit() {
  	this.id = +this.route.snapshot.paramMap.get('id');
  	this.getIntitule();
  	this.getQuestions();
    this.getKeys();
  }

  /**
   * Get the poll name
   */
  getIntitule(): void {
  	this.sondageService.detail(this.id).subscribe(sondage => {
  		this.intitule = sondage.intitule;
  	});
  }

  /**
   * Collect the questions of the id poll
   */
  getQuestions(): void {
  	this.questionsService.list(this.id).subscribe(questions => {
  		this.questions = questions;
  	});
  }

  /**
   * Get the poll public key
   */
  getKeys(): void {
    this.keysService.public(this.id).subscribe(keys => {
      this.keys = keys;
    });
  }

  /**
   * Send the response of an user
   * @param {NgForm} form - The form
   */
  onSubmit(form: NgForm): void {
  	let result: string = '';

  	for(var i = 0; i < this.questions.length; i++) {
  		result += form.value[`question${i}`];
  	}

    // encryption
    var pk = pk_to_jspaillier(this.keys);
    var enc = pk.encrypt(new BigInteger('1')).toString();

    this.reponseService.create(this.id, result, enc).subscribe();

  	this.router.navigate(['/thankyou']);
  }

  getId(): number {
  	return this.id;
  }

  enableSendInc(i: number)
  {
    if(i==0)
      this.inc |= 1;
    else if(i==1)
      this.inc |= 10;
   ;
  }

  enableSend(){
    return (this.inc==11)
  }
}
