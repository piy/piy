import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs/observable/of';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, NgForm, ReactiveFormsModule } from '@angular/forms';

import { RepondreSondageComponent } from './repondre-sondage.component';
import { SondageService } from '../sondage.service';
import { QuestionService } from '../question.service';
import { ReponseService } from '../reponse.service';

describe('RepondreSondageComponent', () => {
	const spySondageService = jasmine.createSpyObj('spySondageService', ['detail']);
	spySondageService.detail.and.returnValue(of({
		id: 1,
		intitule: 'sondage test',
		publie: true,
		termine: false,
		questions: []
	}));

	const spyQuestionService = jasmine.createSpyObj('spyQuestionService', ['list']);
	spyQuestionService.list.and.returnValue(of([
		{
			id: 1,
			num: 1,
			intitule: 'question test'
		}
	]));

	const spyReponseService = jasmine.createSpyObj('spyReponseService', ['create']);
	spyReponseService.create.and.returnValue(of('NO_ERROR'));

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [ FormsModule, ReactiveFormsModule, RouterTestingModule ],
			declarations: [ RepondreSondageComponent ],
			providers: [
				{
					provide: SondageService,
					useValue: spySondageService
				},
				{
					provide: QuestionService,
					useValue: spyQuestionService
				},
				{
					provide: ReponseService,
					useValue: spyReponseService
				}
			],
			schemas: [NO_ERRORS_SCHEMA]
		});
	}));

	let fixture: ComponentFixture<RepondreSondageComponent>;
	let comp: RepondreSondageComponent;

	beforeEach(async(() => {
		fixture = TestBed.createComponent(RepondreSondageComponent);
		comp = fixture.componentInstance;
	}));

	it('should assing intitule when route is resolved', async(() => {
		fixture.detectChanges();

		fixture.whenStable().then(() => {
			 expect(comp.intitule).toBe('sondage test');
		});
	}));

	it('should assign questions when route is resolved', async(() => {
		fixture.detectChanges();

		fixture.whenStable().then(() => {
			expect(comp.questions.length).toBe(1);
			expect(comp.questions[0].id).toBe(1);
			expect(comp.questions[0].num).toBe(1);
			expect(comp.questions[0].intitule).toBe('question test');
		});
	}));

	it('should get a poll', () => {
		fixture.detectChanges();
		comp.getIntitule();
		expect(spySondageService.detail).toHaveBeenCalled();
		expect(spySondageService.detail).toHaveBeenCalledWith(comp.getId());
		expect(comp.intitule).toBe('sondage test');
	})

	it('should recover questions', () => {
		fixture.detectChanges();
		comp.getQuestions();
		expect(spyQuestionService.list).toHaveBeenCalled();
		expect(spyQuestionService.list).toHaveBeenCalledWith(comp.getId());
		expect(comp.questions[0].id).toBe(1);
		expect(comp.questions[0].num).toBe(1);
		expect(comp.questions[0].intitule).toBe('question test');
	});

	it('should have form-group', () => {
		fixture.detectChanges();
		const el = fixture.debugElement.query(By.css('form'));
		expect(el.nativeElement.getElementsByTagName('div').length).toBe(2);
	});
});
