import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs/observable/of'; 

import { apiurl } from './config/config';
import { Page } from './page';
import { Sondage } from './sondage' ;
import { SONDAGES } from './mock-sondages';

const httpOptions = {
  headers: new HttpHeaders({
    'Cache-control': 'no-cache',
    'Pragma': 'no-cache',
    'Expires': 'Sat, 01 Jan 2000 00:00:00 GMT',
  })
};

@Injectable()
export class SondageService {

  constructor(private http: HttpClient) { }

  private url(): string{
    return apiurl + 'sondages/'
  }

  private url_detail(id: number): string{
    return this.url() + id + '/'
  }

  /* Obtient la première page de la liste des sondages en fonction de filtres
     search : recherche par intitule de sondage ("" pour tous)
     page : page à afficher
     ps : taille d'une page
     publie : filtre les sondages publiés ("" pour tous)
     termine: filtre les sondages terminés ("" pour tous)
  */
  list(search="", page=1, ps=1, publie: any ="", termine: any = ""): Observable<Page<Sondage>>{
    var params = "?";
    params += "search=" + search;
    params += "&page=" + page ;
    params += "&ps=" + ps;
    params += "&publie=" + publie;
    params += "&termine=" + termine;
    return this.http.get<Page<Sondage>>(this.url() + params);
  }

  /* Obtient une page de la liste des sondages */
  list_page(url: string): Observable<Page<Sondage>>{
    return this.http.get<Page<Sondage>>(url, httpOptions);
  }

  /* Ajoute un nouveau sondage */
  create(intitule: String): Observable<Sondage>{
    const data = {
      intitule: intitule
    };
    return this.http.post<Sondage>(this.url(), data, httpOptions)
  }

  /* Publie un sondage */
  publish (id: number): Observable<any>{
    return this.http.post(this.url_detail(id)+"publish/", null, httpOptions);
  }

  /* Termine un sondage */
  terminate(id: number): Observable<any>{
    return this.http.post(this.url_detail(id) + "terminate/", null, httpOptions);
  }


  /* Obtient les détails d'un sondage spécifique */
  detail(id: number): Observable<Sondage>{
    return this.http.get<Sondage>(this.url_detail(id), httpOptions)
      .pipe(
        catchError(this.handleError<Sondage>('detail sondage'))
      );
  }

  /* Suprime le sondage */
  delete(id: number): Observable<any>{
    return this.http.delete(this.url_detail(id));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any) => {
      console.error(error);
      return of(result as T);
    }
  }
}
