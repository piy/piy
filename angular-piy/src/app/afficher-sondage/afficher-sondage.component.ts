import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Sondage } from '../sondage';
import { SondageService } from '../sondage.service';
import { Question } from '../question';
import { QuestionService } from '../question.service';
import { Reponse, ReponseClear } from '../reponse';
import { ReponseService } from '../reponse.service';
import { SecretKey, sk_to_jspaillier} from '../keys';
import { KeysService } from '../keys.service';
import { Chart } from 'chart.js';
import 'chart.piecelabel.js';

@Component({
  selector: 'app-afficher-sondage',
  templateUrl: './afficher-sondage.component.html',
  styleUrls: ['./afficher-sondage.component.css']
})
export class AfficherSondageComponent implements OnInit {
  status :  number = 0;
  sondage: Sondage;
  reponses: ReponseClear[] = [];
  keys: any;
  isPublish = false;
  isFinish = false;
  resps: [string, number][] = [];
  total: number = 0;
  chart = [];
  type = "pie";

  constructor(
    private sondageService : SondageService,
    private questionService: QuestionService,
    private reponseService: ReponseService,
    private keysService: KeysService,
    private route: ActivatedRoute
    ) { }

  ngOnInit() {
    this.getSondage();
  }

  getSondage(): void {
    const id = +this.route.snapshot.paramMap.get('id');

    this.sondageService
    .detail(id)
    .subscribe(
      sondage => {
        this.status = 1;
        this.sondage = sondage;

        if(sondage.publie == false){
          this.status = 3;
        } else if (sondage.termine == false){
          this.isPublish = true;
          this.status = 4;
        } else {
          this.isPublish = this.isFinish = true;
          this.getQuestions();
          this.getKey();
        }
      },
      err => {
        if(err.status== 404){
          this.status=2;
        }
      }
      );
  }

  getQuestions(): void {
    this.questionService.list(this.sondage.id)
    .subscribe(questions => this.sondage.questions = questions);
  }

  getKey(): void {
    this.keysService.secret(this.sondage.id)
    .subscribe(
      keys => {
        this.keys = sk_to_jspaillier(keys);

        this.getReponses();
      });
  }

  getReponses(): void {
    this.reponseService.list(this.sondage.id)
    .subscribe(
      reponses => {
        for (const rep of reponses){
          console.log(rep.value);
          const repc = new ReponseClear(this.keys, rep.code, rep.value);
          console.log(repc.value);
          this.reponses.push(repc);
        }
        this.reponsesParQuestions();
        this.totalReponses();
        this.newChart();
      });
  }

  onType(type: string): void {
  	this.type = type;
  	console.log(this.type);
  	this.newChart();
  }

  newChart(): void {
    var label = [];
    var data = [];
    var type = this.type;
    var canvas: any = document.getElementById('canvas');
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);




    this.reponses.forEach((value, index, array) => {
      label.push(this.codeMeaning(value.code));
      data.push(value.value);
    });

    this.chart = new Chart(canvas.getContext('2d'), {
      type: type,
      data: {
        labels: label,
        datasets: [{
          label: 'Résultat sondage',
          data: data,
          backgroundColor: [
          "#3498db",
          "#f1c40f",
          "#e74c3c",
          "#1abc9c"
          ],
          borderColor: [
          "#3498db",
          "#f1c40f",
          "#e74c3c",
          "#1abc9c"
          ],
          borderWidth: [1, 1, 1, 1, 1]
        }]
      },
      options: {
          responsive: true,
          maintainAspectRatio: true,
          pieceLabel: {
              render: 'percentage',
              fontColor: function (data) {
                  function hexToRgb(hex) {
                    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
                    return result ? {
                      r: parseInt(result[1], 16),
                      g: parseInt(result[2], 16),
                      b: parseInt(result[3], 16)
                    } : null;
                  }
                  var rgb = hexToRgb(data.dataset.backgroundColor[data.index]);
                  var threshold = 140;
                  var luminance = 0.299 * rgb.r + 0.587 * rgb.g + 0.114 * rgb.b;
                  return luminance > threshold ? 'black' : 'white';
              },
              precision: 2
          }
      }
    });
  }

  /* Get the meaning of a response code */
  private codeMeaning(code: string): string{
    var mean = "";

    for(var i = 0; i < code.length; i++){
      if (i > 0){
        mean += ", ";
      }

      mean += this.sondage.questions[i].intitule;
      mean += "(";
      if (code[i] == "0"){
        mean += 'non';
      }else{
        mean += 'oui';
      }

      mean += ")";
    }

    return mean;
  }

  reponsesParQuestions(): void {
    for (const reponse of this.reponses){
      //console.log(reponse.code);
      const meaning = this.codeMeaning(reponse.code);
      const value = reponse.value;
      this.resps.push([meaning, value]);
    }
  }

  totalReponses(): void {
    for (const resp of this.reponses){
      this.total += resp.value;
    }
  }

  totaleReponsepParQuestion(question, reponse): number{
    var total = 0;
    for (const resp of this.reponses){

      if(resp.code[question] == reponse){
        total += resp.value;
      }
    }

    return total;
  }
}
