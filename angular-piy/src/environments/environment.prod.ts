export const environment = {
  production: true,
  apiurl: "https://pollityourselfapi.herokuapp.com/api/",
  url: "https://pollityourself.herokuapp.com/"
};
