#!/bin/bash

# Setup django project
echo "SETUP of piy"
echo ""
echo "-----"
echo "SETUP django..."
echo ""
echo "-----"
echo "    SETUP virtualenv"
echo ""

virtualenv env -p python3

echo ""
echo "-----"
echo "    Install python requirements"
echo ""
source ./env/bin/activate
pip install -r requirements.txt
pushd piy

echo ""
echo "-----"
echo "    Migrate database"
echo ""
python ./manage.py migrate
deactivate

echo ""
echo "----"
echo "End of django setup"
popd


# Setup angular
echo ""
echo "-----"
echo "SETUP of angular"
pushd angular-piy/

echo ""
echo "-----"
echo "    Install npm requirements"
npm install

echo ""
echo "-----"
echo "    Create ng command link"
ln -s node_modules/.bin/ng ng
chmod +x ./ng

echo ""
echo "-----"
echo "End of angular setup"
popd

echo ""
echo "-----"
echo "End of piy setup"
