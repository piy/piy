#!/bin/bash

. env/bin/activate
pushd ./piy
python manage.py runserver &
python -c "import piy_worker.worker" &
popd
pushd ./angular-piy
npm start
pkill python
