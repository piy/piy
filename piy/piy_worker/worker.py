import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "piy.settings")
django.setup()

import time
from django.db.models import F
from phe import paillier
from piy_api.models import *

def add(rqueue):
    rep = rqueue.resp
    poll = rep.sondage
    n = int(rqueue.resp.sondage.keys.all()[0].n)
    pk = paillier.PaillierPublicKey(n)

    answer_enc = paillier.EncryptedNumber(pk, int(rqueue.value))
    value_enc = paillier.EncryptedNumber(pk, int(rqueue.resp.value))
    answer_enc += value_enc
    rqueue.resp.value = str(answer_enc.ciphertext())
    rqueue.resp.save()
    rqueue.resp.sondage.nawait = F('nawait') -1
    rqueue.resp.sondage.nagreg = F('nagreg') +1
    rqueue.resp.sondage.save()

def start():
    """ Start the worker
        Okay, this might still have some concurency problems...
        But, with only one worker running at the same time there is no problems.
        TODO change this to be more scalable.
    """
    print("[+] Starting worker")

    while True:
        try:
            r = ReponseHomQueue.objects.all()[0]
            r.delete()
            #print("[+] Processing....")
            try:
                print(r)
                add(r)
            except Exception as e:
                print(e)
        except:
            #print("[-] Nothing to process, sleeping...")
            time.sleep(5)
            pass
        #return


start()
