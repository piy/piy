from rest_framework.exceptions import APIException

class SondagePublishedException(APIException):
    status_code = 423
    default_detail = "You can't do this on a published poll"
    default_code = 'locked'

class SondageNotPublishedException(APIException):
    status_code = 423
    default_detail = "You can't do this on an unpublished poll"
    default_code = 'locked'

class SondageTerminatedException(APIException):
    status_code = 423
    default_detail = "You can't answer to this poll anymore."
    default_code = 'locked'

class SondageNotTerminatedException(APIException):
    status_code = 423
    default_detail = "You can't access this ressource while the poll is not terminated"
    default_code = 'locked'

class SondageNoKeysException(APIException):
	status_code = 410
	default_detail = "This poll has no keys"
	default_code = 'gone'