from django.shortcuts import get_object_or_404
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import detail_route, list_route
from rest_framework.pagination import PageNumberPagination
from rest_framework.filters import OrderingFilter, SearchFilter
from django_filters.rest_framework import DjangoFilterBackend
from django.db.models import F

from phe import paillier

from .models import *
from .serializers import *
from .exceptions import *

class SondageListPagination(PageNumberPagination):
    """ Paramètres de pagination de la liste des sondages """
    page_size = 20
    page_size_query_param = 'ps'
    max_page_size = 100

class SondageViewSet(viewsets.ModelViewSet):
    """
    list:
        Liste les sondages correspondants à un certain filtre.
        Les réponses sont paginées (maximum 100 sondages par page).
        Obtiens uniquement l'id et l'intitule des sondages.

    create:
        Crée un nouveau sondage non publié sans aucune question.

    retrieve:
        Obtiens un unique sondage en fonction de son ID.
        Obtiens l'id, l'intitule, si le sondage est publié et terminé.

    update:
        Change l'intitule du sondage. Uniquement, si le sondage n'est pas publié.
        Retourne le code d'erreur 423 si le sondage est déjà publié.

    partial_update:
        Change l'intitule du sondage. Uniquement, si le sondage n'est pas publié.
        Retourne le code d'erreur 423 si le sondage est déjà publié.

    delete:
        Supprime le sondage, les questions, les réponses et la clé de chiffrement.
    """
    queryset = Sondage.objects.all()
    serializer_class = SondageSerializer
    detail_serializer_class = SondageDetailSerializer
    pagination_class = SondageListPagination
    filter_backends = (OrderingFilter, DjangoFilterBackend, SearchFilter)
    ordering_fields = ('publication', )
    search_fields = ('intitule', )
    filter_fields = ('publie', 'termine')

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return self.detail_serializer_class
        return super(SondageViewSet, self).get_serializer_class()

    def perform_update(self, serializer):
        # block the update of a published poll
        if serializer.instance.publie:
            raise SondagePublishedException()
        serializer.save()

    @detail_route(methods=['post'])
    def publish(self, request, pk):
        """ Publie le sondage.
            La publication du sondage bloque l'édition du sondage
            (impossible d'ajouter des questions, clés)
            et ouvre la réponse aux sondés.
            Il est nécessaire que le sondage contienne deux questions
            et une paire de clés de chiffrement avant de pouvoir être publié.
            Si le sondage ne contient aucune clé de chiffrement, l'erreur 410 est renvoyé.
            Si le sondage ne contient pas exactement deux questions, l'erreur 417 est retourné.
        """
        poll = get_object_or_404(Sondage, pk=pk)
        if len(poll.get_questions()) == 2:
            if len(poll.keys.all()) == 1:
                poll.publie = True
                poll.save()
                return Response(None)
            else:
                raise SondageNoKeysException()
        else:
            return Response({
                'detail': 'A poll need two questions to be published'
            }, status=status.HTTP_417_EXPECTATION_FAILED)

    @detail_route(methods=['post'])
    def terminate(self, request, pk):
        """ Termine le sondage.
            Les sondés ne peuvent plus répondre au sondage.
            Débloque l'accès aux réponses du sondage.
            Il faut que le sondage soit publié pour pouvoir le terminer.
            Le code d'erreur 423 est retourné si le sondage n'est pas publié.
        """
        poll = get_object_or_404(Sondage, pk=pk)
        if poll.publie == False:
            raise SondageNotPublishedException()
        poll.termine = True
        poll.save()
        return Response(None)

class QuestionViewSet(viewsets.ModelViewSet):
    """
    list:
        Liste les questions du sondage.
        La liste des questions est ordonnée.
        Obtiens l'id et l'intitule de chaque réponse.

    create:
        Ajoute une question à la fin de la liste de la liste des questions.
        Le code d'erreur 423 est retourné si le sondage est publié ou terminer.

    retrieve:
        Obtiens une question unique.
        Cette vue n'a pas accès à l'ordre des réponses.

    update:
        Change l'intitulé de la question (si le sondage n'est pas publié).
        Le code d'erreur 423 est retourné si le sondage est publié ou terminé.

    partial_update:
        Change l'intitulé de la question (si le sondage n'est pas publié).
        Le code d'erreur 423 est retourné si le sondage est publié ou terminé.

    delete:
        Supprime la question du sondage (si le sondage n'est pas publié).
        Le code d'erreur 423 est retourné si le sondage est publié ou terminé.
    """
    serializer_class = QuestionSerializer

    def get_queryset(self):
        return Question.objects.filter(sondage=self.kwargs['sondage_pk'])

    def perform_create(self, serializer):
        poll = get_object_or_404(Sondage, id=self.kwargs['sondage_pk'])
        if poll.publie == True:
            raise SondagePublishedException()
        if poll.termine == True:
            raise SondageTerminatedException()

        serializer.save(sondage=poll, num=len(poll.get_questions()))

    def perform_update(self, serializer):
        # block update of a published poll
        if serializer.instance.sondage.publie:
            raise SondagePublishedException()
        serializer.save()

    def perform_destroy(self, instance):
        if instance.sondage.publie == True:
            raise SondagePublishedException()
        instance.delete()

class ReponseViewSet(viewsets.ViewSet):
    serializer_class = ReponseSerializer

    def list(self, request, sondage_pk):
        """ Obtiens les réponses du sondage.
            Les réponses sont uniquement disponibles une fois le sondage terminé.

            Le code d'erreur 423 est renvoyé si la réponse n'est pas disponible.
        """
        poll = get_object_or_404(Sondage, pk=sondage_pk)
        if poll.termine == False:
            raise SondageNotTerminatedException()

        answers = []
        for code in poll.enumerate_responses_codes():
            try:
                answer = poll.reponses.get(code=code)
            except:
                answer = Reponse(code=code, value=0)
            answers.append(answer)

        serializer = ReponseSerializer(answers, many=True)
        return Response(serializer.data)

    def create(self, request, sondage_pk):
        """ Enregistre une réponse au sondage
            code : Code de la réponse. Représente la réponse complète au sondage (à toutes les questions).
            Le code est de la forme "Q1Q2..."
            avec Q1 (et Q2) = (0|1) avec 0: faux et 1: vrai.
            Ex : Q1(Oui) et Q2 (Non) = "10"

            Retourne le code d'erreur 423 si le sondage n'est pas publié.
        """
        poll = get_object_or_404(Sondage, pk=sondage_pk)
        if poll.publie == False:
            raise SondageNotPublishedException()

        serializer = ReponseSerializer(data=request.data)
        if serializer.is_valid():
            code = serializer.validated_data['code']
            value = serializer.validated_data['value']

            if code in poll.enumerate_responses_codes():
                answer, c = poll.reponses.get_or_create(code=code, defaults={'value': 0})
                answer.value += value
                answer.save()
                return Response(None, status=status.HTTP_202_ACCEPTED)
        return Response(None, status=status.HTTP_400_BAD_REQUEST)

class ReponseHomViewSet(viewsets.ViewSet):
    serializer_class = ReponseSerializer

    def list(self, request, sondage_pk):
        """ Obtiens les réponses du sondage.
            Les réponses sont uniquement disponible une fois le sondage terminé.

            Le code d'erreur 423 est renvoyé si la réponse n'est pas disponible.
        """
        poll = get_object_or_404(Sondage, pk=sondage_pk)
        if poll.termine == False:
            raise SondageNotTerminatedException()

        n = int(poll.keys.all()[0].n)
        pk = paillier.PaillierPublicKey(n)

        answers = []
        for code in poll.enumerate_responses_codes():
            try:
                answer = poll.reponses_hom.get(code=code)
            except:
                answer = ReponseHom(code=code, value=pk.encrypt(0).ciphertext())
            answers.append(answer)

        serializer = ReponseHomSerializer(answers, many=True)
        return Response(serializer.data)

    def create(self, request, sondage_pk):
        """ Enregistre une réponse au sondage
            code : Code de la réponse. Représente la réponse complète au sondage (à toutes les questions).
            Le code est de la forme "Q1Q2..."
            avec Q1 (et Q2) = (0|1) avec 0: faux et 1: vrai.
            Ex : Q1(Oui) et Q2 (Non) = "10"

            Retourne le code d'erreur 423 si le sondage n'est pas publié.
        """
        poll = get_object_or_404(Sondage, pk=sondage_pk)
        if poll.publie == False:
            raise SondageNotPublishedException()
        if poll.termine == True:
            raise SondageTerminatedException()

        serializer = ReponseHomSerializer(data=request.data)
        if serializer.is_valid():
            try:
                code = serializer.validated_data['code']
                value = int(serializer.validated_data['value'])

                n = int(poll.keys.all()[0].n)
                pk = paillier.PaillierPublicKey(n)

                if code in poll.enumerate_responses_codes():
                    answer, c = poll.reponses_hom.get_or_create(code=code, defaults={'value': pk.encrypt(0).ciphertext()})
                    ReponseHomQueue.objects.create(resp=answer, value=value)
                    answer.sondage.nawait = F('nawait') +1
                    answer.sondage.save()
                    return Response(None, status=status.HTTP_202_ACCEPTED)
            except:
               pass

        return Response(None, status=status.HTTP_400_BAD_REQUEST)

class KeysViewSet(viewsets.ViewSet):
    def list(self, request, sondage_pk):
        """ Obtiens la paire de clés publique/privée d'un sondage.

            Retourne le code d'erreur 410 si le sondage ne contient aucune clé.
        """
        poll = get_object_or_404(Sondage, pk=sondage_pk)
        try:
            serializer = SecretKeySerializer(poll.keys.all()[0])
            return Response(serializer.data)
        except:
            raise SondageNoKeysException()

    def create(self, request, sondage_pk):
        """ Ajoute une paire de clés au sondage.
            Si le sondage contient déjà une paire de clés,
            elle sera remplacée par cette nouvelle paire de clés.

            Le code d'erreur 423 est retourné si le sondage est publié.
        """
        poll = get_object_or_404(Sondage, pk=sondage_pk)
        if poll.publie:
            raise SondagePublishedException()

        serializer = SecretKeySerializer(data=request.data)
        if serializer.is_valid():
            try: # remove a potential previous key
                poll.keys.all().delete()
            except:
                pass

            try:
                pk = int(serializer.validated_data['n'])
                serializer.save(sondage=poll)
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
            except:
                pass

        return Response(None, status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['get'])
    def public(self, request, sondage_pk):
        """ Obtiens la clé publique du sondage.
            Retourne l'erreur 410 si le sondage ne contient pas de clés.
        """
        poll = get_object_or_404(Sondage, pk=sondage_pk)
        try:
            serializer = PublicKeySerializer(poll.keys.all()[0])
            return Response(serializer.data)
        except:
            raise SondageNoKeysException()

    @list_route(methods=['get'])
    def secret(self, request, sondage_pk):
        """ Obtiens la clé privée du sondage.
            Retourne l'erreur 410 si le sondage ne contient pas de clés.
        """
        poll = get_object_or_404(Sondage, pk=sondage_pk)
        try:
            serializer = SecretKeySerializer(poll.keys.all()[0])
            return Response(serializer.data)
        except:
            raise SondageNoKeysException()
