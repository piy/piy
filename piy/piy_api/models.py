from django.db import models
import itertools
import datetime

class Sondage(models.Model):
    """
    Classe définissant le sondage du :model:`piy_api.models.Sondage`
    """
	# Intitulé du sondage
    intitule = models.CharField(max_length=140, null=False)
    # Sondage publié (non modifiable et répondable)
    publie = models.BooleanField(default=False)
    # Sondage terminer (publié mais avec les réponses bloqué)
    termine = models.BooleanField(default=False)
    # Date de publication
    publication = models.DateField(auto_now_add=True)
    # Nombre total de réponses en attente d'être agrégée (pas représentative du nombre de sondé)
    nawait = models.IntegerField(default=0)
    # Nombre total de réponses agrégée (pas représentative du nombre de sondé)
    nagreg = models.IntegerField(default=0)

    def get_questions(self):
        """
        Obtient toutes les questions du sondage.
        Trie les questions dans l'ordre
        """
        return Question.objects.filter(sondage=self).order_by('num')

    def enumerate_responses_codes(self):
        """ Get all the responses codes for this poll as an iterator of """
        nq = len(self.get_questions())
        codes = []

        for code in itertools.product('01', repeat=nq):
            sc = ""
            for e in code:
                sc += e
            codes.append(sc)

        return codes

class Keys(models.Model):
    """ Paire de clés (paillier) d'un sondage """
    # Sondage
    sondage = models.ForeignKey(Sondage, related_name='keys', on_delete=models.CASCADE)
    # taille de la clé (en bits)
    size = models.IntegerField()
    # clé publique (encodée sous forme d'entiers en base 10)
    n = models.CharField(max_length=1024)
    # clé secrete (potentiellement chiffrée coté client).
    lmbda = models.CharField(max_length=1024)
    # Vecteur d'initialisation (IV) utilisé en cas de chiffrement de la clé privée coté client
    iv = models.CharField(max_length=64, null=True, default=None)

class Question(models.Model):
    """ Question appartenant à un sondage
    """
    # Sondage propriétaire de la question
    sondage = models.ForeignKey(Sondage, related_name='questions', on_delete=models.CASCADE)
    # intitule de la question
    intitule = models.CharField(max_length=140, null=False)
    # Numerot de la question
    num = models.IntegerField(null=False)

    class Meta:
        unique_together = (('sondage', 'num'))

class Reponse(models.Model):
    """ Représente une réponse possible à un ensemble de question du sondage """
    # Sondage
    sondage = models.ForeignKey(Sondage, related_name='reponses', on_delete=models.CASCADE)
    # Code de la réponse. Représente la réponse complête au sondage (à toutes les questions).
    # Le code est de la forme "Q1Q2..."
    # avec Q1 (et Q2) = (0|1) avec 0: faux et 1: vrai.
    # Ex : Q1(Oui) et Q2 (Non) = "10"
    code = models.CharField(max_length=32, null=False, blank=False)
    # nombre de réponses
    value = models.IntegerField(null=False)

class ReponseHom(models.Model):
    """ Représente une réponse possible à un ensemble de questions d'un sondage.
        La value de la réponse est chiffrée homomorphiquement avec la clé publique du sondage.
    """
    # Sondage
    sondage = models.ForeignKey(Sondage, related_name='reponses_hom', on_delete=models.CASCADE)
    # Code de la réponse. Représente la réponse complête au sondage (à toutes les questions).
    # Le code est de la forme "Q1Q2..."
    # avec Q1 (et Q2) = (0|1) avec 0: faux et 1: vrai.
    # Ex : Q1(Oui) et Q2 (Non) = "10"
    code = models.CharField(max_length=32, null=False, blank=False)
    # Valeur (chiffrée) de la réponse (somme des réponses individuelles)
    value = models.CharField(max_length=1024)

class ReponseHomQueue(models.Model):
    """ Représente une réponse possible à un ensemble de questions d'un sondage.
        La value de la réponse est chiffrée homomorphiquement avec la clé publique du sondage.
    """
    # Where to addd the value
    resp = models.ForeignKey(ReponseHom, related_name='queue', on_delete=models.CASCADE)
    # Valeur (chiffrée) de la réponse (somme des réponses individuelles)
    value = models.CharField(max_length=1024)
