from django.conf.urls import url, include
from django.urls import path
from rest_framework_nested import routers
from rest_framework.documentation import include_docs_urls

from .views import *

router = routers.SimpleRouter()
router.register(r'sondages', SondageViewSet)

sondages_router = routers.NestedSimpleRouter(router, r'sondages', lookup='sondage')
sondages_router.register(r'questions', QuestionViewSet, base_name='sondage-questions')
#sondages_router.register(r'reponses', ReponseViewSet, base_name='sondage-reponses')
sondages_router.register(r'reponses', ReponseHomViewSet, base_name='sondage-reponseshom')
sondages_router.register(r'keys', KeysViewSet, base_name='sondage-keys')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^', include(sondages_router.urls)),
    url(r'^docs/', include_docs_urls(title='Poll It Yourself API'), name="api-docs")
]
