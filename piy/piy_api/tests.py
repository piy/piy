from django.test import TestCase
from rest_framework.reverse import reverse
from rest_framework.test import APIClient
from phe import paillier
import json

client = APIClient()

class ApiSondage:
    """ Interface vers SondageViewSet """

    def url(self):
        return reverse('sondage-list')

    def url_detail(self, id):
        return reverse('sondage-detail', args=[id])

    def url_publish(self, id):
        return reverse('sondage-detail', args=[id]) + 'publish/'

    def url_terminate(self, id):
        return reverse('sondage-detail', args=[id]) + 'terminate/'

    def list(self):
        return client.get(self.url(), format='json')

    def page(self, url):
        return client.get(url, format='json')

    def create(self, intitule):
        params = {
            "intitule": intitule
        }
        return client.post(self.url(), params, format='json')

    def update(self, id, intitule):
        params = {
            "intitule": intitule
        }
        return client.put(self.url_detail(id), params, format='json')

    def partial_update(self, id, intitule):
        params = {
            "intitule": intitule
        }
        return client.patch(self.url_detail(id), params, format='json')

    def publish(self, id):
        return client.post(self.url_publish(id), None, format='json')

    def terminate(self, id):
        return client.post(self.url_terminate(id), None, format='json')

    def retrieve(self, id):
        return client.get(self.url_detail(id), format='json')

    def update(self, id, intitule):
        params = {
            "intitule": intitule,
        }
        return client.put(self.url_detail(id), params, format='json')

    def partial_update(self, id, intitule):
        params = {
            "intitule": intitule,
        }
        return client.patch(self.url_detail(id), params, format='json')

    def delete(self, id):
        return client.delete(self.url_detail(id), format='json')

class ApiQuestion:
    """ Interface vers QuestionViewSet """

    def url(self, sondage):
        return reverse('sondage-questions-list', args=[sondage])

    def url_detail(self, sondage, id):
        return reverse('sondage-questions-detail', args=[sondage, id])

    def list(self, sondage):
        return client.get(self.url(sondage), format='json')

    def create(self, sondage, intitule):
        params = {
            "intitule": intitule
        }
        return client.post(self.url(sondage), params, format='json')

    def retrieve(self, sondage, id):
        return client.get(self.url_detail(sondage, id), format='json')

    def update(self, sondage, id, intitule):
        params = {
            "intitule": intitule,
        }
        return client.put(self.url_detail(sondage, id), params, format='json')

    def partial_update(self, sondage, id, intitule):
        params = {
            "intitule": intitule,
        }
        return client.patch(self.url_detail(sondage, id), params, format='json')

    def delete(self, sondage, id):
        return client.delete(self.url_detail(sondage, id), format='json')

class ApiReponse:
    """ Interface vers ReponseViewSet """

    def url(self, sondage):
        return reverse('sondage-reponses-list', args=[sondage])

    def list(self, sondage):
        return client.get(self.url(sondage), format='json')

    def create(self, sondage, code, value):
        params = {
            'code': code,
            'value': value
        }
        return client.post(self.url(sondage), params, format='json')

class ApiReponseHom:
    """ Interface vers ReponseHomViewSet """

    def url(self, sondage):
        return reverse('sondage-reponseshom-list', args=[sondage])

    def list(self, sondage):
        return client.get(self.url(sondage), format='json')

    def create(self, sondage, code, value):
        params = {
            'code': code,
            'value': value
        }
        return client.post(self.url(sondage), params, format='json')

class ApiKeys:
    """ Interface vers KeysViewSet """

    def url(self, sondage):
        return reverse('sondage-keys-list', args=[sondage])

    def url_public(self, sondage):
        return self.url(sondage) + 'public/'

    def url_private(self, sondage):
        return self.url(sondage) + 'secret/'

    def list(self, sondage):
        return client.get(self.url(sondage), format='json')

    def create(self, sondage, size, pk, sk, iv=None):
        params = {
            'size': size,
            'n': pk,
            'lmbda': sk,
            'iv': iv
        }
        return client.post(self.url(sondage), params, format='json')

    def public(self, sondage):
        return client.get(self.url_public(sondage), format='json')

    def private(self, sondage):
        return  client.get(self.url_private(sondage), format='json')

class TestsSondage(TestCase):
    """ Cas de tests pour SondageViewSet """
    sondages = ApiSondage()
    questions = ApiQuestion()
    keys = ApiKeys()

    def test_add_pool_empty(self):
        res = self.sondages.create("")
        self.assertEqual(res.status_code, 400)

    def test_add_pool(self):
        res = self.sondages.create("WAZA")
        self.assertEqual(res.status_code, 201)

    def test_list_empty(self):
        res = self.sondages.list()
        self.assertEqual(res.status_code, 200)
        self.assertEqual(json.loads(res.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': []
        })

    def test_list_one(self):
        self.sondages.create("WAZA")

        res = self.sondages.list()
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.data, {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'id': 1,
                'intitule': 'WAZA'
            }]
        })

    def test_list_many(self):
        self.sondages.create("WAZA")
        self.sondages.create("WAZA2")

        res = self.sondages.list()
        self.assertEqual(res.status_code, 200)
        self.assertEqual(json.loads(res.content), {
            'count': 2,
            'next': None,
            'previous': None,
            'results' : [{
                'id': 1,
                'intitule': 'WAZA'
            },{
                'id': 2,
                'intitule': 'WAZA2'
            }]
        })

    def test_list_page(self):
        self.maxDiff = 2048
        results = []
        for x in range(25):
            self.sondages.create(str(x))
            results.append({
                    'id': x +1,
                    'intitule': str(x)
                })

        res = self.sondages.list()
        self.assertEqual(res.status_code, 200)
        page1 = json.loads(res.content)
        self.assertEqual(page1, {
            'count': 25,
            'next': "http://testserver" + self.sondages.url() + '?page=2',
            'previous': None,
            'results': results[:20]
        })

        res = self.sondages.page(page1['next'])
        self.assertEqual(res.status_code, 200)
        self.assertEqual(json.loads(res.content), {
            'count': 25,
            'next': None,
            'previous': "http://testserver" + self.sondages.url(),
            'results': results[20:]
        })


    def test_publish_nexist(self):
        res = self.sondages.publish(1)
        self.assertEqual(res.status_code, 404)

    def test_publish_zero_questions(self):
        res = self.sondages.create("WAZA")
        id = res.data['id']

        res = self.sondages.publish(id)
        self.assertEqual(res.status_code, 417)

    def test_publish_one_questions(self):
        res = self.sondages.create("WAZA")
        id = res.data['id']
        self.questions.create(id, "Q1?")

        res = self.sondages.publish(id)
        self.assertEqual(res.status_code, 417)

    def test_publish_no_keys(self):
        res = self.sondages.create("WAZA")
        id = res.data['id']
        self.questions.create(id, "Q1?")
        self.questions.create(id, "Q2?")
        res = self.sondages.publish(id)
        self.assertEqual(res.status_code, 410)

    def test_publish_two_questions(self):
        res = self.sondages.create("WAZA")
        id = res.data['id']
        self.questions.create(id, "Q1?")
        self.questions.create(id, "Q2?")

        self.keys.create(id, 8, 143, 60)

        res = self.sondages.publish(id)
        self.assertEqual(res.status_code, 200)

    def test_publish_three_questions(self):
        res = self.sondages.create("WAZA")
        id = res.data['id']
        self.questions.create(id, "Q1?")
        self.questions.create(id, "Q2?")
        self.questions.create(id, "Q3?")

        res = self.sondages.publish(id)
        self.assertEqual(res.status_code, 417)

    def test_terminate_nexist(self):
        res = self.sondages.terminate(42)
        self.assertEqual(res.status_code, 404)

    def test_terminate_npublie(self):
        res = self.sondages.create("WAZA")
        id = res.data['id']
        res = self.sondages.terminate(id)
        self.assertEqual(res.status_code, 423)

    def test_terminate(self):
        res = self.sondages.create("WAZA")
        id = res.data['id']
        self.questions.create(id, "Q1?")
        self.questions.create(id, "Q2?")
        self.keys.create(id, 8, 143, 60)
        res = self.sondages.publish(id)
        res = self.sondages.terminate(id)
        self.assertEqual(res.status_code, 200)

    def test_update_nexist(self):
        res = self.sondages.update(1, "WAZABI")
        self.assertEqual(res.status_code, 404)

    def test_update(self):
        res = self.sondages.create("WAZA")
        id = res.data['id']
        res = self.sondages.update(id, "WAZABI")
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.data, {
            'id': id,
            'intitule': "WAZABI"
        })

    def test_update_published(self):
        res = self.sondages.create("WAZA")
        id = res.data['id']
        self.questions.create(id, "Q1?")
        self.questions.create(id, "Q2?")
        self.keys.create(id, 8, 143, 60)
        res = self.sondages.publish(id)
        res = self.sondages.update(id, "WAZABI")
        self.assertEqual(res.status_code, 423)

    def test_update_empty(self):
        res = self.sondages.create("WAZA")
        id = res.data['id']
        res = self.sondages.update(id, "")
        self.assertEqual(res.status_code, 400)

    def test_partial_update_nexist(self):
        res = self.sondages.partial_update(1, "WAZABI")
        self.assertEqual(res.status_code, 404)

    def test_partial_update(self):
        res = self.sondages.create("WAZA")
        id = res.data['id']
        res = self.sondages.partial_update(id, "WAZABI")
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.data, {
            'id': id,
            'intitule': "WAZABI"
        })

    def test_partial_update_published(self):
        res = self.sondages.create("WAZA")
        id = res.data['id']
        self.questions.create(id, "Q1?")
        self.questions.create(id, "Q2?")
        self.keys.create(id, 8, 143, 60)
        res = self.sondages.publish(id)
        res = self.sondages.partial_update(id, "WAZABI")
        self.assertEqual(res.status_code, 423)

    def test_partial_update_empty(self):
        res = self.sondages.create("WAZA")
        id = res.data['id']
        res = self.sondages.partial_update(id, "")
        self.assertEqual(res.status_code, 400)

    def test_retrieve_nexist(self):
        res = self.sondages.retrieve(1)
        self.assertEqual(res.status_code, 404)

    def test_retrieve(self):
        res = self.sondages.create("WAZA")
        id = res.data['id']
        res = self.sondages.retrieve(id)

        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.data, {
            'id': id,
            'intitule': 'WAZA',
            'publie': False,
            'termine': False,
            'nawait': 0,
            'nagreg': 0
        })

    def test_delete_nexist(self):
        res = self.sondages.delete(1)
        self.assertEqual(res.status_code, 404)

    def test_delete(self):
        res = self.sondages.create("WAZA")
        id = res.data['id']
        res = self.sondages.delete(id)
        self.assertEqual(res.status_code, 204)

        res = self.sondages.list()
        self.assertEqual(res.status_code, 200)
        self.assertEqual(json.loads(res.content), {
            'count': 0,
            'next': None,
            'previous': None,
            'results': []
        })

class TestsQuestion(TestCase):
    """ Cas de tests pour QuestionViewSet """

    sondages = ApiSondage()
    questions = ApiQuestion()
    keys = ApiKeys()

    def setUp(self):
        res = self.sondages.create("WAZA")
        self.sid = res.data['id']

    def test_add_nexist_poll(self):
        res = self.questions.create(42, "e")
        self.assertEqual(res.status_code, 404)

    def test_add_empty(self):
        res = self.questions.create(self.sid, "")
        self.assertEqual(res.status_code, 400)

    def test_add(self):
        res = self.questions.create(self.sid, "Q1?")
        self.assertEqual(res.status_code, 201)
        self.assertEqual(res.data, {
            'id': 1,
            'intitule': "Q1?"
        })

    def test_add_published(self):
        self.questions.create(self.sid, "Q1?")
        self.questions.create(self.sid, "Q2?")
        self.keys.create(self.sid, 8, 143, 60)
        self.sondages.publish(self.sid)
        res = self.questions.create(self.sid, "Q2?")
        self.assertEqual(res.status_code, 423)

    def test_list_empty(self):
        res = self.questions.list(self.sid)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.data, [])

    def test_list_one(self):
        self.questions.create(self.sid, "Q1?")
        res = self.questions.list(self.sid)

        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.data, [{
            'id': 1,
            'intitule': "Q1?"
        }])

    def test_list_many(self):
        self.questions.create(self.sid, "Q1?")
        self.questions.create(self.sid, "Q2?")
        res = self.questions.list(self.sid)

        self.assertEqual(res.status_code, 200)
        self.assertEqual(json.loads(res.content), [{
            'id': 1,
            'intitule': "Q1?"
        }, {
            'id': 2,
            'intitule': "Q2?"
        }])

    def test_update_nexist(self):
        res = self.questions.update(self.sid, 1, "nQ1?")
        self.assertEqual(res.status_code, 404)

    def test_update_empty(self):
        res = self.questions.create(self.sid, "Q1?")
        res = self.questions.update(self.sid, 1, "")
        self.assertEqual(res.status_code, 400)

    def test_update_published(self):
        self.questions.create(self.sid, "Q1?")
        self.questions.create(self.sid, "Q1?")
        self.keys.create(self.sid, 8, 143, 60)
        self.sondages.publish(self.sid)
        res = self.questions.update(self.sid, 1, "nQ1?")
        self.assertEqual(res.status_code, 423)

    def test_update(self):
        res = self.questions.create(self.sid, "Q1?")
        res = self.questions.update(self.sid, 1, "nQ1?")
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.data, {
            'id': 1,
            'intitule': 'nQ1?'
        })

    def test_partial_update_nexist(self):
        res = self.questions.partial_update(self.sid, 1, "nQ1?")
        self.assertEqual(res.status_code, 404)

    def test_partial_update_empty(self):
        res = self.questions.create(self.sid, "Q1?")
        res = self.questions.partial_update(self.sid, 1, "")
        self.assertEqual(res.status_code, 400)

    def test_partial_update_published(self):
        self.questions.create(self.sid, "Q1?")
        self.questions.create(self.sid, "Q1?")
        self.keys.create(self.sid, 8, 143, 60)
        self.sondages.publish(self.sid)
        res = self.questions.partial_update(self.sid, 1, "nQ1?")
        self.assertEqual(res.status_code, 423)

    def test_partial_update(self):
        res = self.questions.create(self.sid, "Q1?")
        res = self.questions.partial_update(self.sid, 1, "nQ1?")
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.data, {
            'id': 1,
            'intitule': 'nQ1?'
        })


    def test_retrieve_nexist(self):
        res = self.questions.retrieve(self.sid, 42)
        self.assertEqual(res.status_code, 404)

    def test_retrieve(self):
        self.questions.create(self.sid, "Q1?")
        res = self.questions.retrieve(self.sid, 1)

        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.data, {
            'id': 1,
            'intitule': "Q1?"
        })

    def test_delete_nexist(self):
        res = self.questions.delete(self.sid, 42)
        self.assertEqual(res.status_code, 404)

    def test_delete(self):
        self.questions.create(self.sid, "Q1?")
        res = self.questions.delete(self.sid, 1)

        self.assertEqual(res.status_code, 204)

    def test_add_published(self):
        self.questions.create(self.sid, "Q1?")
        self.questions.create(self.sid, "Q2?")
        self.keys.create(self.sid, 8, 143, 60)
        self.sondages.publish(self.sid)
        res = self.questions.delete(self.sid, 1)
        self.assertEqual(res.status_code, 423)


#class TestsReponse(TestCase):
    """Cas de tests de ReponseViewSet """
    """

    sondages = ApiSondage()
    questions = ApiQuestion()
    reponses = ApiReponse()
    keys = ApiKeys()

    def setUp(self):
        res = self.sondages.create("WAZA")
        self.sid = res.data['id']

    def test_list_nexist_poll(self):
        res = self.reponses.list(42)
        self.assertEqual(res.status_code, 404)

    def test_add_nexist_poll(self):
        res = self.reponses.create(42, "00", 0)
        self.assertEqual(res.status_code, 404)

    def test_list_no_questions(self):
        res = self.reponses.list(self.sid)
        self.assertEqual(res.status_code, 423)

    def test_list_mono_question_empty(self):
        self.questions.create(self.sid, "1?")

        res = self.reponses.list(self.sid)
        self.assertEqual(res.status_code, 423)

    def test_list_bi_question_empty(self):
        self.questions.create(self.sid, "1?")
        self.questions.create(self.sid, "2?")
        self.keys.create(self.sid, 8, 143, 60)
        self.sondages.publish(self.sid)
        self.sondages.terminate(self.sid)
        res = self.reponses.list(self.sid)

        self.assertEqual(res.status_code, 200)
        self.assertEqual(json.loads(res.content), [{
            'code': "00",
            'value': 0
        }, {
            'code': "01",
            'value': 0
        }, {
            'code': "10",
            'value': 0
        }, {
            'code': "11",
            'value': 0
        }])

    def test_list_bi_question_ntermine(self):
        self.questions.create(self.sid, "1?")
        self.questions.create(self.sid, "2?")
        self.keys.create(self.sid, 8, 143, 60)
        self.sondages.publish(self.sid)

        res = self.reponses.list(self.sid)
        self.assertEqual(res.status_code, 423)

    def test_list_unpublished(self):
        self.questions.create(self.sid, "1?")
        self.questions.create(self.sid, "2?")

        res = self.reponses.list(self.sid)
        self.assertEqual(res.status_code, 423)

    def test_create_mono(self):
        self.questions.create(self.sid, "1?")

        res = self.reponses.create(self.sid, "0", 42)
        self.assertEqual(res.status_code, 423)

    def test_create_unpublished(self):
        self.questions.create(self.sid, "1?")
        self.questions.create(self.sid, "2?")

        res = self.reponses.create(self.sid, "00", 1)
        self.assertEqual(res.status_code, 423)

    def test_create_bi_question(self):
        self.questions.create(self.sid, "1?")
        self.questions.create(self.sid, "2?")
        self.keys.create(self.sid, 8, 143, 60)
        self.sondages.publish(self.sid)

        res = self.reponses.create(self.sid, "00", 1)
        self.assertEqual(res.status_code, 202)

        self.sondages.terminate(self.sid)

        res = self.reponses.list(self.sid)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(json.loads(res.content), [{
            'code': "00",
            'value': 1
        }, {
            'code': "01",
            'value': 0
        }, {
            'code': "10",
            'value': 0
        }, {
            'code': "11",
            'value': 0
        }])

    def test_create_inv_code(self):
        self.questions.create(self.sid, "1?")
        self.questions.create(self.sid, "2?")
        self.keys.create(self.sid, 8, 143, 60)
        self.sondages.publish(self.sid)

        res = self.reponses.create(self.sid, "hoho", 42)
        self.assertEqual(res.status_code, 400)
"""

class TestsReponseHom(TestCase):
    """ Cas de tests de ReponseHomViewSet """

    sondages = ApiSondage()
    questions = ApiQuestion()
    reponses = ApiReponseHom()
    keys = ApiKeys()

    def setUp(self):
        res = self.sondages.create("WAZA")
        self.sid = res.data['id']
        self.size = 8
        self.n = 143
        self.lmbda = 60
        self.pk = paillier.PaillierPublicKey(143)
        res = self.keys.create(self.sid, self.size, self.n, self.lmbda)

    def enc(self, i):
        return str(self.pk.encrypt(i).ciphertext())

    def test_list_nexist_poll(self):
        res = self.reponses.list(42)
        self.assertEqual(res.status_code, 404)

    def test_add_nexist_poll(self):
        res = self.reponses.create(42, "00", 0)
        self.assertEqual(res.status_code, 404)

    def test_list_no_questions(self):
        res = self.reponses.list(self.sid)
        self.assertEqual(res.status_code, 423)

    def test_list_mono_question_empty(self):
        self.questions.create(self.sid, "1?")

        res = self.reponses.list(self.sid)
        self.assertEqual(res.status_code, 423)

    def test_list_bi_question_empty(self):
        self.questions.create(self.sid, "1?")
        self.questions.create(self.sid, "2?")
        self.sondages.publish(self.sid)
        self.sondages.terminate(self.sid)
        res = self.reponses.list(self.sid)
        self.assertEqual(res.status_code, 200)

    def test_list_bi_question_pending(self):
        self.questions.create(self.sid, "1?")
        self.questions.create(self.sid, "2?")
        self.sondages.publish(self.sid)
        res = self.reponses.list(self.sid)
        self.assertEqual(res.status_code, 423)

    def test_list_unpublished(self):
        self.questions.create(self.sid, "1?")
        self.questions.create(self.sid, "2?")

        res = self.reponses.list(self.sid)
        self.assertEqual(res.status_code, 423)

    def test_create_mono(self):
        self.questions.create(self.sid, "1?")

        res = self.reponses.create(self.sid, "0", 42)
        self.assertEqual(res.status_code, 423)

    def test_create_unpublished(self):
        self.questions.create(self.sid, "1?")
        self.questions.create(self.sid, "2?")

        res = self.reponses.create(self.sid, "00", 1)
        self.assertEqual(res.status_code, 423)

    def test_create_bi_question(self):
        self.questions.create(self.sid, "1?")
        self.questions.create(self.sid, "2?")
        self.sondages.publish(self.sid)

        v = self.enc(1)
        res = self.reponses.create(self.sid, "00", v)
        self.assertEqual(res.status_code, 202)

        self.sondages.terminate(self.sid)
        res = self.reponses.list(self.sid)
        self.assertEqual(res.status_code, 200)

    def test_create_inv_code(self):
        self.questions.create(self.sid, "1?")
        self.questions.create(self.sid, "2?")
        self.sondages.publish(self.sid)

        res = self.reponses.create(self.sid, "hoho", 42)
        self.assertEqual(res.status_code, 400)

class TestKeys(TestCase):
    """ Cas de tests pour KeysViewSet """

    sondages = ApiSondage()
    keys = ApiKeys()

    def setUp(self):
        res = self.sondages.create("WAZA")
        self.sid = res.data['id']
        self.size = 8
        self.n = '143'
        self.lmbda = '60'
        self.iv = 'IV'

    def test_create_nexist(self):
        res = self.keys.create(42, self.size, self.n, self.lmbda)
        self.assertEqual(res.status_code, 404)

    def test_create_nonint(self):
        res = self.keys.create(self.sid, self.size, "42n", self.lmbda)
        self.assertEqual(res.status_code, 400)

    def test_create(self):
        res = self.keys.create(self.sid, self.size, self.n, self.lmbda)
        self.assertEqual(res.status_code, 202)

    def test_create_iv(self):
        res = self.keys.create(self.sid, self.size, self.n, self.lmbda, self.iv)
        self.assertEqual(res.status_code, 202)
        res = self.keys.list(self.sid)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(json.loads(res.content), {
            'size': self.size,
            'n': self.n,
            'lmbda': self.lmbda,
            'iv': self.iv
        })

    def test_list_nexist(self):
        res = self.keys.list(42)
        self.assertEqual(res.status_code, 404)

    def test_list_nokeys(self):
        res = self.keys.list(self.sid)
        self.assertEqual(res.status_code, 410)

    def test_list(self):
        res = self.keys.create(self.sid, self.size, self.n, self.lmbda)
        res = self.keys.list(self.sid)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(json.loads(res.content), {
            'size': self.size,
            'n': self.n,
            'lmbda': self.lmbda,
            'iv': None
        })

    def test_public_nexist(self):
        res = self.keys.public(42)
        self.assertEqual(res.status_code, 404)

    def test_public_nokey(self):
        res = self.keys.public(self.sid)
        self.assertEqual(res.status_code, 410)

    def test_public(self):
        res = self.keys.create(self.sid, self.size, self.n, self.lmbda)
        res = self.keys.public(self.sid)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(json.loads(res.content), {
            'size': self.size,
            'n': self.n
        })

    def test_private_nexist(self):
        res = self.keys.private(42)
        self.assertEqual(res.status_code, 404)

    def test_private_nokey(self):
        res = self.keys.private(self.sid)
        self.assertEqual(res.status_code, 410)

    def test_private(self):
        res = self.keys.create(self.sid, self.size, self.n, self.lmbda)
        res = self.keys.private(self.sid)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(json.loads(res.content), {
            'size': self.size,
            'n': self.n,
            'lmbda': self.lmbda,
            'iv': None
        })
