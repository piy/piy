from rest_framework import serializers

from .models import *

class SondageSerializer(serializers.ModelSerializer):
    """ Serialize les meta-informations (id, intitule) d'un sondage """

    class Meta:
        model = Sondage
        fields = ('id', 'intitule')

class SondageDetailSerializer(serializers.ModelSerializer):
    """ Serialize les informations détaillées (id, intitule, etat) d'un sondage """
    class Meta:
        model = Sondage
        fields = ('id', 'intitule', 'publie', 'termine', 'nawait', 'nagreg')

class QuestionSerializer(serializers.ModelSerializer):
    """ Serialize une question d'un sondage """

    class Meta:
        model = Question
        fields = ('id', 'intitule')

class ReponseSerializer(serializers.ModelSerializer):
    """ Serialize un réponse à un ensemble de questions d'un sondage """

    class Meta:
        model = Reponse
        fields = ('code', 'value')

class ReponseHomSerializer(serializers.ModelSerializer):
    """ Serialize un réponse (chiffrée) à un ensemble de questions d'un sondage """

    class Meta:
        model = ReponseHom
        fields = ('code', 'value')

class PublicKeySerializer(serializers.ModelSerializer):
    """ Serialize une clé publique """
    class Meta:
        model = Keys
        fields = ('size', 'n')

class SecretKeySerializer(serializers.ModelSerializer):
    """ Serialize une pair de clé publique/privée """
    class Meta:
        model = Keys
        fields = ('size', 'n', 'lmbda', 'iv')
