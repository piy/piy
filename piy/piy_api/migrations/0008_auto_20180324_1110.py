# Generated by Django 2.0.1 on 2018-03-24 11:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('piy_api', '0007_auto_20180304_1803'),
    ]

    operations = [
        migrations.AlterField(
            model_name='question',
            name='sondage',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='questions', to='piy_api.Sondage'),
        ),
        migrations.AlterField(
            model_name='reponse',
            name='sondage',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='reponses', to='piy_api.Sondage'),
        ),
    ]
