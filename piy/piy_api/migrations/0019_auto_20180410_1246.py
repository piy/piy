# Generated by Django 2.0.1 on 2018-04-10 12:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('piy_api', '0018_keys_iv'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='reponse',
            name='sondage',
        ),
        migrations.DeleteModel(
            name='Reponse',
        ),
    ]
