# Generated by Django 2.0.1 on 2018-03-25 14:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('piy_api', '0008_auto_20180324_1110'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reponse',
            name='Q1Q2',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='reponse',
            name='Q1nQ2',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='reponse',
            name='nQ1Q2',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='reponse',
            name='nQ1nQ2',
            field=models.IntegerField(default=0),
        ),
    ]
