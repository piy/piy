from django.apps import AppConfig


class PiyApiConfig(AppConfig):
    name = 'piy_api'
