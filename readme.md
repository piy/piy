# Pool It Yourself (PIY)

## Installation

Dépendences:
  - python 3
  - virtualenv
  - npm
  - gcc, linux-headers, gmp-dev, mpfr-dev, mpc1-dev (selon les systèmes) 
  - docker (optionel - dépolyment)

Pour installer le projet (sur un unixoïde).
Le script `setup.sh` initialize virtualenv dans `./env/`, télécharge les dépendences,
et applique les migrations.

```
git clone git@gitlab.inria.fr:piy/piy.git
./setup.sh
```

## Organisation du projet

Le projet angular(front-end) se trouve dans le dossier `angular-piy`.
Le projet django(api rest) se trouve dans le dossier `piy`.
Ce dossier contient un fichier `manage.py` permettant d'effectuer des actions
sur le projet (migration de la base de donnée, lancer un serveur local, ...).
Le dossier `piy` contient des fichiers de configurations globaux du projet (a ne pas toucher donc).
Le dossier `piy_api` contient le code relatif à l'API.
Dans ce dossier le dossier `migrations` contient les changements dans la base de donnée (a ne pas toucher).
Le fichier `models` contient le modèle de la base de donnée.
Le fichier `urls.py` contient les routes vers les vues.
Le fichier `views.py` contient les vues elles mêmes.

Les urls (paths) relatifs à l'API seront sous l'url `/api/`.

## Activer virtualenv

Si le projet à été installé via le script `setup.sh`:
```
(à la racine du projet)
souce ./env/bin/activate
```

## Lancer un serveur de test local

Le script `./start.sh` démare un serveur de test django et angular

```
./start.sh
```

## Déployment en production

Avec [Heroku] (http://heroku.com/) :

```
cd piy
heroku container:push web -a pollityourselfapi 
cd ../angular_piy
heroku container:push web -a pollityourself
```